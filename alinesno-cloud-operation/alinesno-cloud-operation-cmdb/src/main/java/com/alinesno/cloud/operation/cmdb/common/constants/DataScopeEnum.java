package com.alinesno.cloud.operation.cmdb.common.constants;

/**
 * 数据范围 
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午6:41:23
 */
public enum DataScopeEnum {
	DATA_ALL("all" , "全部数据权限") , 
	DATA_AREA("area" , "区域数据权限")  ; 

	private String code;
	private String text ;
	
	DataScopeEnum(String code , String text) {
		this.code = code;
		this.text = text ; 
	}
	
	public static DataScopeEnum getStatus(String code) {
		if("all".equals(code)) {
			return DataScopeEnum.DATA_ALL; 
		}else if("area".equals(code)) {
			return DataScopeEnum.DATA_AREA; 
		}
		return DATA_AREA ; 
	}
	
	public String getText() {
		return this.text ; 
	}

	public String getCode() {
		return this.code;
	}
}