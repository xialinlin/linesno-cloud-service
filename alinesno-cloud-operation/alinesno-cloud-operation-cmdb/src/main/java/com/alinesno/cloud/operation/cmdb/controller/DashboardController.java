package com.alinesno.cloud.operation.cmdb.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * 登陆控制层
 * @author LuoAnDong
 * @since 2018年8月17日 上午7:48:06
 */
@Controller
public class DashboardController extends BaseController {

	/**
	 * 进入主面板
	 * 
	 * @param model
	 * @return
	 */
	@GetMapping("/")
	public String index(Model model) {
		return redirect("/manager/dashboard") ;
	}
	
}
