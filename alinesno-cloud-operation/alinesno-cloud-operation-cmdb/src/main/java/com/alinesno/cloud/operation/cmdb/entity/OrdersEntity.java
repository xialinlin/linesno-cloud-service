package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * 商品订单
 * @author LuoAnDong
 * @since 2018年8月5日 下午12:04:10
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_orders")
public class OrdersEntity extends BaseEntity {

	private String orderId ; //订单id
	private String sendStatus ; //发送状态
	private String infoId ; //订单详情
	private String orderEvaluation ; //订单评价
	private String receiveManagerId ; //配送员
	private String salaId ; //销售员
	private String userId ; //下单用户
	
	@Lob
	private String errorMsg ; //错误原因

	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getSalaId() {
		return salaId;
	}
	public void setSalaId(String salaId) {
		this.salaId = salaId;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getSendStatus() {
		return sendStatus;
	}
	public void setSendStatus(String sendStatus) {
		this.sendStatus = sendStatus;
	}
	public String getInfoId() {
		return infoId;
	}
	public void setInfoId(String infoId) {
		this.infoId = infoId;
	}
	public String getOrderEvaluation() {
		return orderEvaluation;
	}
	public void setOrderEvaluation(String orderEvaluation) {
		this.orderEvaluation = orderEvaluation;
	}
	public String getReceiveManagerId() {
		return receiveManagerId;
	}
	public void setReceiveManagerId(String receiveManagerId) {
		this.receiveManagerId = receiveManagerId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

}
