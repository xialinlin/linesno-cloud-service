package com.alinesno.cloud.operation.cmdb.controller.pages;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alinesno.cloud.operation.cmdb.controller.BaseController;

/**
 * 门户服务 
 * @author LuoAnDong
 * @since 2019年3月19日 下午12:00:32
 */
@Controller
@RequestMapping("/pages")
public class PortalController extends BaseController {
	
	@GetMapping("/portal")
	public String list(Model model) {
		return WX_PAGE+"portal";
	}	
}
