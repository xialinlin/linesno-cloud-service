package com.alinesno.cloud.operation.cmdb.web.bean;

import com.alinesno.cloud.operation.cmdb.entity.OrderRecordEntity;

/**
 * 订单详情
 * @author LuoAnDong
 * @since 2018年8月24日 下午10:33:22
 */
@SuppressWarnings("serial")
public class OrderRecordEntityExt extends OrderRecordEntity {

	private String userName ;  //用户名
	private String phone ; //手机号
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
}
