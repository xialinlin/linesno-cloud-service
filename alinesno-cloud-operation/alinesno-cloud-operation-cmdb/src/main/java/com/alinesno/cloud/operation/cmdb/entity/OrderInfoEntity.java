package com.alinesno.cloud.operation.cmdb.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alibaba.fastjson.annotation.JSONField;

/**
 * 订单信息
 * @author LuoAnDong
 * @since 2018年8月5日 上午11:46:07
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_order_info")
public class OrderInfoEntity extends BaseEntity {

	private String goodsName ; // 服务用途
	private String goodsOrderId ; // 任务详情(与goods表关联)
	private String goodsType  ; // 任务类型(01饭堂/02水果/03零食/99其它)
	private String goodsWeight ; //货物重量(以kg为单位)

	private String remarks ; //备注
	private String businessType ; //业务类型(1帮我买/2帮我取/3帮我送/4代排队)

	private String getAddress ; //开始地址
	private String receiveAddress ; //收货地址
	private String receivePhone ; //收货人手机

	private String priceCert ; //是否凭小票(1是/0否)

	private String userId ; //用户主键
	private String floor ; //所在楼层

	private String totalPay ; //计算支付费用
	private int discount ; //打折(按百分比,80即80%)

	// 服务器申请内容
	private String orderCpu ; // cpu
	private String orderStorage ; //存储
	private String orderMemory ; //内存
	private String orderNetwork ; //网络
	private String orderSystem ; //操作系统
	private String orderUsername ; //用户名
	private String orderPwd ; //申请密码

	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date sendTime ; // 申请时间
	
	@JSONField(format = "yyyy-MM-dd HH:mm:ss")
	private Date endTime ; // 申请完成时间 

	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public String getOrderCpu() {
		return orderCpu;
	}
	public void setOrderCpu(String orderCpu) {
		this.orderCpu = orderCpu;
	}
	public String getOrderStorage() {
		return orderStorage;
	}
	public void setOrderStorage(String orderStorage) {
		this.orderStorage = orderStorage;
	}
	public String getOrderMemory() {
		return orderMemory;
	}
	public void setOrderMemory(String orderMemory) {
		this.orderMemory = orderMemory;
	}
	public String getOrderNetwork() {
		return orderNetwork;
	}
	public void setOrderNetwork(String orderNetwork) {
		this.orderNetwork = orderNetwork;
	}
	public String getOrderSystem() {
		return orderSystem;
	}
	public void setOrderSystem(String orderSystem) {
		this.orderSystem = orderSystem;
	}
	public String getOrderUsername() {
		return orderUsername;
	}
	public void setOrderUsername(String orderUsername) {
		this.orderUsername = orderUsername;
	}
	public String getOrderPwd() {
		return orderPwd;
	}
	public void setOrderPwd(String orderPwd) {
		this.orderPwd = orderPwd;
	}
	public String getGoodsOrderId() {
		return goodsOrderId;
	}
	public void setGoodsOrderId(String goodsOrderId) {
		this.goodsOrderId = goodsOrderId;
	}
	public String getGoodsName() {
		return goodsName;
	}
	public void setGoodsName(String goodsName) {
		this.goodsName = goodsName;
	}
	public String getGoodsType() {
		return goodsType;
	}
	public void setGoodsType(String goodsType) {
		this.goodsType = goodsType;
	}
	public String getGoodsWeight() {
		return goodsWeight;
	}
	public void setGoodsWeight(String goodsWeight) {
		this.goodsWeight = goodsWeight;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getBusinessType() {
		return businessType;
	}
	public void setBusinessType(String businessType) {
		this.businessType = businessType;
	}
	public String getGetAddress() {
		return getAddress;
	}
	public void setGetAddress(String getAddress) {
		this.getAddress = getAddress;
	}
	public String getReceiveAddress() {
		return receiveAddress;
	}
	public void setReceiveAddress(String receiveAddress) {
		this.receiveAddress = receiveAddress;
	}
	public String getReceivePhone() {
		return receivePhone;
	}
	public void setReceivePhone(String receivePhone) {
		this.receivePhone = receivePhone;
	}
	public String getPriceCert() {
		return priceCert;
	}
	public void setPriceCert(String priceCert) {
		this.priceCert = priceCert;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getSendTime() {
		return sendTime;
	}
	public void setSendTime(Date sendTime) {
		this.sendTime = sendTime;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getTotalPay() {
		return totalPay;
	}
	public void setTotalPay(String totalPay) {
		this.totalPay = totalPay;
	}
	public int getDiscount() {
		return discount;
	}
	public void setDiscount(int discount) {
		this.discount = discount;
	}
	@Override
	public String toString() {
		return "OrderInfoEntity [goodsName=" + goodsName + ", goodsType=" + goodsType + ", goodsWeight=" + goodsWeight
				+ ", remarks=" + remarks + ", businessType=" + businessType + ", getAddress=" + getAddress
				+ ", receiveAddress=" + receiveAddress + ", receivePhone=" + receivePhone + ", priceCert=" + priceCert
				+ ", userId=" + userId + ", sendTime=" + sendTime + ", floor=" + floor
				+ ", totalPay=" + totalPay + ", discount=" + discount + "]";
	}
	
}
