package com.alinesno.cloud.operation.cmdb.controller.manager;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.operation.cmdb.advice.ConvertCode;
import com.alinesno.cloud.operation.cmdb.controller.BaseController;
import com.alinesno.cloud.operation.cmdb.entity.LinksEntity;
import com.alinesno.cloud.operation.cmdb.repository.LinksRepository;
import com.alinesno.cloud.operation.cmdb.web.bean.JqDatatablesPageBean;
import com.alinesno.cloud.operation.cmdb.web.bean.ResponseBean;
import com.alinesno.cloud.operation.cmdb.web.bean.ResultGenerator;

/**
 * 后台主面板
 * @author LuoAnDong
 * @since 2018年8月14日 下午12:58:05
 */
@Controller
@RequestMapping("/manager/")
public class ManagerLinksController extends BaseController{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 
	
	@Autowired
	private LinksRepository linksRepository ; 

	@GetMapping("/links_list")
	public String list(Model model) {
		
		createMenus(model); 
		return WX_MANAGER+"links_list";
	}
	
	@GetMapping("/links_add")
	public String add(Model model) {
		return WX_MANAGER+"links_add";
	}
	
	@GetMapping("/links_modify/{id}")
	public String paramsModify(Model model , @PathVariable("id") String id) {
		logger.debug("id = {}" , id);
		
		LinksEntity bean = linksRepository.findById(id).get() ; 
		model.addAttribute("bean" , bean) ; 
		
		return WX_MANAGER+"links_modify";
	}
	
	/**
	 * 数据列表
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ConvertCode
	@ResponseBody
	@GetMapping("/links_list_data")
	public JqDatatablesPageBean paramsListData(Model model , JqDatatablesPageBean page) {
		
		return this.toPage(model, 
				page.buildFilter(LinksEntity.class , request)  , 
				linksRepository, 
				page); 
	}
	
	
	 
	@ResponseBody
	@PostMapping("/links_save")
	public ResponseBean<String> save(Model model , LinksEntity e) {
		logger.debug("e = {}" , ToStringBuilder.reflectionToString(e));
		linksRepository.save(e) ; 
		e.setMasterCode(currentManager().getMasterCode());
		return ResultGenerator.genSuccessMessage("保存成功");
	}
}
