package com.alinesno.cloud.operation.cmdb.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-01-23
 */
@SuppressWarnings("serial")
@Entity
@Table(name="cmdb_sms_fail")
public class SmsFailEntity extends BaseEntity {

  
	private String validateCode; // 验证码
	private String content; // 短信内容
	private String busnessId; // 业务id
	private String signName; // 签名
	private String templateCode; // 模板代码
	private String template; // 模板内容
	private String phone; // 发送手机
	private String outId ; //内容扩展名
	private String bizId ; //返回的业务id
	
	public String getValidateCode() {
		return validateCode;
	}
	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getBusnessId() {
		return busnessId;
	}
	public void setBusnessId(String busnessId) {
		this.busnessId = busnessId;
	}
	public String getSignName() {
		return signName;
	}
	public void setSignName(String signName) {
		this.signName = signName;
	}
	public String getTemplateCode() {
		return templateCode;
	}
	public void setTemplateCode(String templateCode) {
		this.templateCode = templateCode;
	}
	public String getTemplate() {
		return template;
	}
	public void setTemplate(String template) {
		this.template = template;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getOutId() {
		return outId;
	}
	public void setOutId(String outId) {
		this.outId = outId;
	}
	public String getBizId() {
		return bizId;
	}
	public void setBizId(String bizId) {
		this.bizId = bizId;
	}

}
