package com.alinesno.cloud.operation.cmdb.controller.manager;

import java.sql.Timestamp;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alinesno.cloud.operation.cmdb.advice.ConvertCode;
import com.alinesno.cloud.operation.cmdb.controller.BaseController;
import com.alinesno.cloud.operation.cmdb.entity.ParamsEntity;
import com.alinesno.cloud.operation.cmdb.repository.ParamsRepository;
import com.alinesno.cloud.operation.cmdb.web.bean.JqDatatablesPageBean;
import com.alinesno.cloud.operation.cmdb.web.bean.ResponseBean;
import com.alinesno.cloud.operation.cmdb.web.bean.ResultGenerator;

/**
 * 后台主面板
 * @author LuoAnDong
 * @since 2018年8月14日 下午12:58:05
 */
@Controller
@RequestMapping("/manager/")
public class ManagerParamsController extends BaseController{
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass()) ; 
	
	@Autowired
	private ParamsRepository paramsRepository ; 

	@GetMapping("/params_list")
	public String list(Model model) {
		
		createMenus(model); 
		return WX_MANAGER+"params_list";
	}
	
	@GetMapping("/params_modify/{id}")
	public String paramsModify(Model model , @PathVariable("id") String id) {
		logger.debug("id = {}" , id);
		
		ParamsEntity bean = paramsRepository.findById(id).get() ; 
		model.addAttribute("bean" , bean) ; 
		
		return WX_MANAGER+"params_modify";
	}
	
	/**
	 * 数据列表
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@ConvertCode
	@ResponseBody
	@GetMapping("/params_list_data")
	public JqDatatablesPageBean paramsListData(Model model , JqDatatablesPageBean page) {
//		return this.toPage(model, paramsRepository, page); 
		
		return this.toPage(model, 
				page.buildFilter(ParamsEntity.class , request)  , 
				paramsRepository, 
				page); 
	}
	
	
	 
	@ResponseBody
	@PostMapping("/params_save")
	public ResponseBean<String> save(Model model , ParamsEntity e) {
		logger.debug("e = {}" , ToStringBuilder.reflectionToString(e));
		
		ParamsEntity bean = paramsRepository.findById(e.getId()).get() ; 
		bean.setUpdateTime(new Timestamp(System.currentTimeMillis()));
		bean.setParamDesc(e.getParamDesc());
		bean.setParamName(e.getParamName());
		bean.setParamValue(e.getParamValue());
		
		paramsRepository.save(bean) ; 
		return ResultGenerator.genSuccessMessage("保存成功");
	}
}
