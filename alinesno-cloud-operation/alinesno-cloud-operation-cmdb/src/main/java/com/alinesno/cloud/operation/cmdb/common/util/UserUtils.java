package com.alinesno.cloud.operation.cmdb.common.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alinesno.cloud.operation.cmdb.entity.UserEntity;

/**
 * 用户工具对象
 * 
 * @author LuoAnDong
 * @since 2018年11月2日 下午9:42:04
 */
public class UserUtils {

	private static final Logger logger = LoggerFactory.getLogger(UserUtils.class);

	/**
	 * 获取用户姓名
	 * 
	 * @param u
	 * @return
	 */
	public static String realName(UserEntity u) {
		String realNameStr = "镖师";

		try {
			if (u != null) {
				String phone = u.getPhone() ;
				String realName = u.getRealName() ;
				realNameStr = StringUtils.isNotBlank(realName) ? realName + "(" + phone + ")" : phone;
			}
		} catch (Exception e) {
			logger.error("获取用户信息失败:{}", e);
		}

		return realNameStr;
	}

	/**
	 * 名字隐藏
	 * 
	 * @param str
	 * @return
	 */
	@SuppressWarnings("unused")
	private static String replaceNameX(String str) {
		String reg = ".{1}";
		StringBuffer sb = new StringBuffer();
		Pattern p = Pattern.compile(reg);
		Matcher m = p.matcher(str);
		int i = 0;
		while (m.find()) {
			i++;
			if (i == 1)
				continue;
			m.appendReplacement(sb, "*");
		}
		m.appendTail(sb);
		return sb.toString();
	}

}
