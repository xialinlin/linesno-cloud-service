/**
* jquery菜单
*/
;(function ($) {
    $.fn.menus = function (options) {
    	
    	var defaults={
    		url: options.url, // 菜单链接  
    		params: options.params,
    		parentId: options.parentId ,
    		target: options.target // 生成HTML的目标id 
		};
		var endOptions=$.extend(defaults,options); 
	
		var navHtml = '<ul class="nav">' ; 
		
		this.each(function () { //实现功能的代码
			$.ajax({
				type:'GET',
			    url: endOptions.url ,
			    data: endOptions.params ,
			    success: function(response){
			        console.log('返回的数据: ' + response);
			        if(response.code != 200 || response.data == null){
			        	layer.alert(response.message==null?"返回数据失败":response.message) ;
			        	return ; 
			        }
			        
			        var data = response.data ; 
			        for(var i = 0 ; i < data.length ; i ++){
			        	var item = data[i] ; 
						if(item.menuType == 1){
							navHtml += '<li class="divider"></li><li class="nav-title">'+item.resourceName+'</li>' ; 
						}
						
			        	var itemList = item.subResource ; 
			        	if(itemList != null){
							for(var j = 0 ; j < itemList.length ; j ++){
								
								var itemListSub = itemList[j] ; 
								var itemListSubList = itemListSub.subResource ; 
								
								if(itemListSubList != null && itemListSubList.length > 0){
								
									var subNavHtml = '<ul class="nav-dropdown-items">' ;
									for(var n = 0 ; n < itemListSubList.length ; n ++){
										subNavHtml += 
										'<li class="nav-item">' +
											'<a class="nav-link" target="main" href="'+itemListSubList[n].resourceLink+'"> <i class="nav-icon '+ itemListSubList[n].resourceIcon +'"></i>'+ itemListSubList[n].resourceName + '</a>' +
										'</li>' ; 
									}
									subNavHtml += '</ul>' ;
									
									navHtml += 
									'<li class="nav-item nav-dropdown">'+
										'<a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon '+ itemListSub.resourceIcon +'"></i>'+ itemListSub.resourceName + '</a>'+
										subNavHtml + 
									'</li>' ; 
								}else{
									navHtml += '<li class="nav-item">'+
										'<a class="nav-link" target="main" href="'+itemListSub.resourceLink+'">'+
											'<i class="nav-icon '+ itemListSub.resourceIcon +'"></i>'+ itemListSub.resourceName +  
										'</a>'+
									'</li>' ; 
								}
							}
			        	}
						
			        }
			        // console.log(navHtml) ; 
					navHtml += '</ul>' ; 
					$(endOptions.target).html(navHtml) ; 
					
					$(".item,.nav-dropdown").click(function(){
						console.log('.nav-item .nav-dropdown') ;
						$(this).siblings("li").removeClass("open") ;
					});
	
			    }
			});
        });
    };
})(jQuery);

// 初始化菜单 
$(function(){
	
	/*
	$('.sidebar-nav').menus({
		url:'boot/platform/menus/side',
		params:'resourceParent=536478251871109120',
		target:'.sidebar-nav'
	}) ; 
	*/
	var applicationId = $("a#applicationItem").first().data("id") ; 
	showApplicationMenus(self , applicationId) ; 

	// 菜单点击
	//$('.nav-link').on('click', menuItem);
	
});


//显示应用菜单
function showApplicationMenus(self , applicationId){
	var id = $(self).data("id") ; 
	console.log("application id = " + applicationId) ;
	if(applicationId !== null && typeof(applicationId) != "undefined"){
		id = applicationId ; 
	}
	$('.sidebar-nav').menus({
		url:'dashboard/side',
		params:'applicationId='+id ,
		target:'.sidebar-nav'
	}) ; 
}

function menuItem() {
    // 获取标识数据
    var dataUrl = $(this).attr('href'),
    dataIndex = $(this).data('index'),
    menuName = $.trim($(this).text()),
    flag = true;
    $(".nav ul li").removeClass("active");
    $(this).parent("li").addClass("active");
    if (dataUrl == undefined || $.trim(dataUrl).length == 0) return false;

    // 选项卡菜单已存在
    $('.menuTab').each(function() {
        if ($(this).data('id') == dataUrl) {
            if (!$(this).hasClass('active')) {
                $(this).addClass('active').siblings('.menuTab').removeClass('active');
                scrollToTab(this);
                // 显示tab对应的内容区
                $('.mainContent .RuoYi_iframe').each(function() {
                    if ($(this).data('id') == dataUrl) {
                        $(this).show().siblings('.RuoYi_iframe').hide();
                        return false;
                    }
                });
            }
            flag = false;
            return false;
        }
    });
    // 选项卡菜单不存在
    if (flag) {
        var str = '<a href="javascript:;" class="active menuTab" data-id="' + dataUrl + '">' + menuName + ' <i class="fa fa-times-circle"></i></a>';
        $('.menuTab').removeClass('active');

        // 添加选项卡对应的iframe
        var str1 = '<iframe class="RuoYi_iframe" name="iframe' + dataIndex + '" width="100%" height="100%" src="' + dataUrl + '" frameborder="0" data-id="' + dataUrl + '" seamless></iframe>';
        $('.mainContent').find('iframe.RuoYi_iframe').hide().parents('.mainContent').append(str1);
        
        $.modal.loading("数据加载中，请稍后...");
        
        $('.mainContent iframe:visible').load(function () {
        	$.modal.closeLoading();
        });
        
        // 添加选项卡
        $('.menuTabs .page-tabs-content').append(str);
        scrollToTab($('.menuTab.active'));
    }
    return false;
}