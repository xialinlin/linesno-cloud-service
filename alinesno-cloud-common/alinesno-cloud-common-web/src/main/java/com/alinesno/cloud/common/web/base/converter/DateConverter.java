package com.alinesno.cloud.common.web.base.converter;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.converter.Converter;

/**
 * springmvc 时间转换工具类
 * 
 * @author luodong
 *
 */
public class DateConverter implements Converter<String, Date> {

	// 日志记录
	private final static Logger logger = LoggerFactory.getLogger(DateConverter.class);

	private static final String[] DATA_PATTERNS = { "yyyy-MM-dd", "yyyyMMdd", "yyyy-MM-dd HH:mm" };

	@Override
	public Date convert(String source) {

		if (logger.isDebugEnabled()) {
			logger.debug("date converter = {}", source);
		}

		if (StringUtils.isBlank(source))
			return null;

		try {
			return DateUtils.parseDate(source, DATA_PATTERNS);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return null;
	}
}