package com.alinesno.cloud.common.web.base.api;
/**
 * 接口数据请求的类型
 * @author LuoAnDong
 * @date 2017年12月1日 下午9:01:56
 */
public enum ReturnType {
	JSON("json") , 
	XML("xml") , 
	REDITRECT("redirect")  ; 
	
	private String value ; 
	
	ReturnType(String value) {
		this.value = value ; 
	}
	
	String value() {
		return value ; 
	}
}