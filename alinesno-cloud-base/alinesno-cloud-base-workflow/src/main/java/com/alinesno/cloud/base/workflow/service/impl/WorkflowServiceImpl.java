package com.alinesno.cloud.base.workflow.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;

import com.alinesno.cloud.base.workflow.bean.ActivityDefineEntity;
import com.alinesno.cloud.base.workflow.bean.ActivityInstEntity;
import com.alinesno.cloud.base.workflow.bean.ApproveBean;
import com.alinesno.cloud.base.workflow.bean.BusinessBean;
import com.alinesno.cloud.base.workflow.bean.BusinessTransactionBean;
import com.alinesno.cloud.base.workflow.bean.WorkItemBean;
import com.alinesno.cloud.base.workflow.service.WorkflowService;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * 工作流实例 
 * @author LuoAnDong
 * @since 2019年4月17日 下午6:14:34
 */
public class WorkflowServiceImpl implements WorkflowService {

	@Override
	public ResponseEntity<?> confirmBiz(ApproveBean sbVo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> approveBiz(ApproveBean spVo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<BusinessBean> searchTodoTasks(RestWrapper wrapper, String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<BusinessBean> searchTodoTaskForPage(RestWrapper wrapper, Page<BusinessBean> page, String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTodoTaskIns(String processDefName, String userId, String... lcjds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<WorkItemBean> getTodoTasks(String processDefName, String userId, String... lcjds) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<String> getNextNodes(long processDefId, String activityDefId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActivityDefineEntity> getNextActNodes(long processDefId, String activityDefId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ActivityDefineEntity> getNextActNodes(long rootProcessInstID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BusinessTransactionBean createProcess(String processDefName, String userId, String xpath, Object relaData) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BusinessTransactionBean createProcess(String processDefName, String userId, boolean isFinishFirst) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BusinessTransactionBean finishWorkItem(long processInstId, long workItemId, String xpath, Object relaData,
			String userId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BusinessTransactionBean createProcess(String processDefName, String userId,
			List<Map<String, Object>> relaDataMaps) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ActivityInstEntity createAndGotoSpecifiedActivity(long processInstId, String userId, String activityDefID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ActivityInstEntity createAndGotoSpecifiedSubActivity(long processInstId, String userId, String activityDefID,
			String subActDefID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WorkItemBean createAndGotoSpecifiedActivity(String processDefName, String userId, String activityDefID) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public WorkItemBean getTodoTaskByProcessInstId(Long processInstId, String userId) {
		// TODO Auto-generated method stub
		return null;
	}

}
