package com.alinesno.cloud.base.logger.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;
import java.util.Date;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:16:06
 */
@SuppressWarnings("serial")
public class LogLoginDto extends BaseDto {

    /**
     * 登陆ip
     */
	private String loginIp;
	
    /**
     * 登陆时间
     */
	private Date loginTime;
	
    /**
     * 退出时间
     */
	private Date logoutTime;
	
    /**
     * 登陆用户
     */
	private String loginUser;
	
    /**
     * 其它登陆信息
     */
	private String loginMsg;
	


	public String getLoginIp() {
		return loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public Date getLoginTime() {
		return loginTime;
	}

	public void setLoginTime(Date loginTime) {
		this.loginTime = loginTime;
	}

	public Date getLogoutTime() {
		return logoutTime;
	}

	public void setLogoutTime(Date logoutTime) {
		this.logoutTime = logoutTime;
	}

	public String getLoginUser() {
		return loginUser;
	}

	public void setLoginUser(String loginUser) {
		this.loginUser = loginUser;
	}

	public String getLoginMsg() {
		return loginMsg;
	}

	public void setLoginMsg(String loginMsg) {
		this.loginMsg = loginMsg;
	}

}
