package com.alinesno.cloud.base.boot.feign.dto;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class ManagerSearchDto extends BaseDto {

    /**
     * 资源Id
     */
	private String resourceId;
	
    /**
     * 显示名称
     */
	private String label;
	
    /**
     * 字段名称
     */
	private String name;
	
    /**
     * 字段属性
     */
	private String optionJson;
	
    /**
     * 操作类型(input/select),默认为input
     */
	private String searchType;
	
    /**
     * 默认值
     */
	private String defaultValue;
	
    /**
     * 是否为日期(空则不是|否则是)
     */
	private String hasDate;
	


	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOptionJson() {
		return optionJson;
	}

	public void setOptionJson(String optionJson) {
		this.optionJson = optionJson;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public String getHasDate() {
		return hasDate;
	}

	public void setHasDate(String hasDate) {
		this.hasDate = hasDate;
	}

}
