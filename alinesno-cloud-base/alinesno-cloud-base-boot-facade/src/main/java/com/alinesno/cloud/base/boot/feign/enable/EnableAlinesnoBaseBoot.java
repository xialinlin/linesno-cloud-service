package com.alinesno.cloud.base.boot.feign.enable;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Import;

/**
 * 实现基础服务
 * 
 * @author LuoAnDong
 * @sine 2019年4月5日 上午11:36:41
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
@EnableFeignClients(basePackages = "com.alinesno.cloud.**.*.feign.facade") // 自动引入所有包
@Import({BaseBootConfigurationSelector.class})
public @interface EnableAlinesnoBaseBoot {

	// //扫描feign包下的，变成接口可调用包

	/**
	 * If true, the ServiceRegistry will automatically register the local server.
	 */
	boolean autoRegister() default true;
	
}
