package com.alinesno.cloud.base.boot.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;

import com.alinesno.cloud.base.boot.feign.dto.ManagerSourceGenerateDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-17 07:44:37
 */
@FeignClient(name="alinesno-cloud-base-boot" , path="managerSourceGenerate")
public interface ManagerSourceGenerateFeigin extends IBaseFeign<ManagerSourceGenerateDto> {

}
