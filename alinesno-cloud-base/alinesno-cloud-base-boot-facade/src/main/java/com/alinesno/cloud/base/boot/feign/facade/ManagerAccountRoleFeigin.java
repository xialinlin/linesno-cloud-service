package com.alinesno.cloud.base.boot.feign.facade;

import org.springframework.cloud.openfeign.FeignClient;

import com.alinesno.cloud.base.boot.feign.dto.ManagerAccountRoleDto;
import com.alinesno.cloud.common.facade.feign.IBaseFeign;

/**
 * <p>  请求客户端 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-08 08:14:52
 */
@FeignClient(name="alinesno-cloud-base-boot" , path="managerAccountRole")
public interface ManagerAccountRoleFeigin extends IBaseFeign<ManagerAccountRoleDto> {

}
