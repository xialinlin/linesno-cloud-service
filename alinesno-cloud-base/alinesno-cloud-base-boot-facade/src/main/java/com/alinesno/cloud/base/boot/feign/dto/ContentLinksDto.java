package com.alinesno.cloud.base.boot.feign.dto;

import java.util.Date;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 18:00:40
 */
@SuppressWarnings("serial")
public class ContentLinksDto extends BaseDto {

    /**
     * 内容链接
     */
	private String linkUrl;
	
    /**
     * 内容链接名称
     */
	private String linkName;
	
    /**
     * 链接图片
     */
	private String linkImage;
	
    /**
     * 链接打开目标
     */
	private String linkTarget;
	
    /**
     * 链接描述
     */
	private String linkDescription;
	
    /**
     * 链接是否可见
     */
	private String linkVisible;
	
    /**
     * 链接作者
     */
	private Long linkOwner;
	
    /**
     * 链接评分
     */
	private Integer linkRating;
	
    /**
     * 链接更新时间 
     */
	private Date linkUpdated;
	
    /**
     * 标签
     */
	private String linkNotes;
	


	public String getLinkUrl() {
		return linkUrl;
	}

	public void setLinkUrl(String linkUrl) {
		this.linkUrl = linkUrl;
	}

	public String getLinkName() {
		return linkName;
	}

	public void setLinkName(String linkName) {
		this.linkName = linkName;
	}

	public String getLinkImage() {
		return linkImage;
	}

	public void setLinkImage(String linkImage) {
		this.linkImage = linkImage;
	}

	public String getLinkTarget() {
		return linkTarget;
	}

	public void setLinkTarget(String linkTarget) {
		this.linkTarget = linkTarget;
	}

	public String getLinkDescription() {
		return linkDescription;
	}

	public void setLinkDescription(String linkDescription) {
		this.linkDescription = linkDescription;
	}

	public String getLinkVisible() {
		return linkVisible;
	}

	public void setLinkVisible(String linkVisible) {
		this.linkVisible = linkVisible;
	}

	public Long getLinkOwner() {
		return linkOwner;
	}

	public void setLinkOwner(Long linkOwner) {
		this.linkOwner = linkOwner;
	}

	public Integer getLinkRating() {
		return linkRating;
	}

	public void setLinkRating(Integer linkRating) {
		this.linkRating = linkRating;
	}

	public Date getLinkUpdated() {
		return linkUpdated;
	}

	public void setLinkUpdated(Date linkUpdated) {
		this.linkUpdated = linkUpdated;
	}

	public String getLinkNotes() {
		return linkNotes;
	}

	public void setLinkNotes(String linkNotes) {
		this.linkNotes = linkNotes;
	}

}
