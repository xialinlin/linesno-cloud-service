package com.alinesno.cloud.base.message.feign.dto;

import java.util.Date;

import com.alinesno.cloud.common.facade.feign.BaseDto;

/**
 * <p> 传输对象</p>
 *
 * @author LuoAnDong
 * @since 2019-06-08 09:28:52
 */
@SuppressWarnings("serial")
public class TransactionMessageHistoryDto extends BaseDto {

    /**
     * 是否死亡
     */
	private Integer areadlyDead;
	
    /**
     * 消息内容
     */
	private String businessBody;
	
    /**
     * 消息数据类型
     */
	private String businessDateType;
	
    /**
     * 业务主键
     */
	private String businessId;
	
	private String consumerQueue;
	
    /**
     * 创建时间
     */
	private Date createTime;
	
    /**
     * 创建人
     */
	private String creater;
	
    /**
     * 编辑时间
     */
	private Date editTime;
	
    /**
     * 编辑人
     */
	private String editor;
	
    /**
     * 扩展字段1
     */
	private String field1;
	
    /**
     * 扩展字段2
     */
	private String field2;
	
    /**
     * 扩展字段3
     */
	private String field3;
	
    /**
     * 消息发送时间
     */
	private Integer messageSendTimes;
	
    /**
     * 消息状态
     */
	private String messageStatus;
	
    /**
     * 消息备注
     */
	private String remark;
	
    /**
     * 消息版本
     */
	private Integer versions;
	
    /**
     * 消息主题
     */
	private String topic;
	


	public Integer getAreadlyDead() {
		return areadlyDead;
	}

	public void setAreadlyDead(Integer areadlyDead) {
		this.areadlyDead = areadlyDead;
	}

	public String getBusinessBody() {
		return businessBody;
	}

	public void setBusinessBody(String businessBody) {
		this.businessBody = businessBody;
	}

	public String getBusinessDateType() {
		return businessDateType;
	}

	public void setBusinessDateType(String businessDateType) {
		this.businessDateType = businessDateType;
	}

	public String getBusinessId() {
		return businessId;
	}

	public void setBusinessId(String businessId) {
		this.businessId = businessId;
	}

	public String getConsumerQueue() {
		return consumerQueue;
	}

	public void setConsumerQueue(String consumerQueue) {
		this.consumerQueue = consumerQueue;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	public Date getEditTime() {
		return editTime;
	}

	public void setEditTime(Date editTime) {
		this.editTime = editTime;
	}

	public String getEditor() {
		return editor;
	}

	public void setEditor(String editor) {
		this.editor = editor;
	}

	public String getField1() {
		return field1;
	}

	public void setField1(String field1) {
		this.field1 = field1;
	}

	public String getField2() {
		return field2;
	}

	public void setField2(String field2) {
		this.field2 = field2;
	}

	public String getField3() {
		return field3;
	}

	public void setField3(String field3) {
		this.field3 = field3;
	}

	public Integer getMessageSendTimes() {
		return messageSendTimes;
	}

	public void setMessageSendTimes(Integer messageSendTimes) {
		this.messageSendTimes = messageSendTimes;
	}

	public String getMessageStatus() {
		return messageStatus;
	}

	public void setMessageStatus(String messageStatus) {
		this.messageStatus = messageStatus;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getVersions() {
		return versions;
	}

	public void setVersions(Integer versions) {
		this.versions = versions;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

}
