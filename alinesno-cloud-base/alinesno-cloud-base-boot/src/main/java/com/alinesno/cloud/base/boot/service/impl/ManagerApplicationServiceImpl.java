package com.alinesno.cloud.base.boot.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.boot.entity.ManagerAccountEntity;
import com.alinesno.cloud.base.boot.entity.ManagerAccountRoleEntity;
import com.alinesno.cloud.base.boot.entity.ManagerApplicationEntity;
import com.alinesno.cloud.base.boot.entity.ManagerResourceEntity;
import com.alinesno.cloud.base.boot.entity.ManagerRoleEntity;
import com.alinesno.cloud.base.boot.enums.MenuEnums;
import com.alinesno.cloud.base.boot.enums.ResourceTypeEnmus;
import com.alinesno.cloud.base.boot.repository.ManagerApplicationRepository;
import com.alinesno.cloud.base.boot.service.IManagerAccountRoleService;
import com.alinesno.cloud.base.boot.service.IManagerAccountService;
import com.alinesno.cloud.base.boot.service.IManagerApplicationService;
import com.alinesno.cloud.base.boot.service.IManagerResourceService;
import com.alinesno.cloud.base.boot.service.IManagerRoleService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Service
public class ManagerApplicationServiceImpl extends IBaseServiceImpl<ManagerApplicationRepository, ManagerApplicationEntity, String> implements IManagerApplicationService {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(ManagerApplicationServiceImpl.class);

	@Autowired
	private IManagerRoleService managerRoleService ; 
	
	@Autowired
	private IManagerResourceService managerResourceService ; 
	
	@Autowired
	private IManagerAccountRoleService managerAccountRoleService ; 
	
	@Autowired
	private IManagerAccountService managerAccountService ; 

	@Transactional
	@SuppressWarnings("unchecked")
	@Override
	public ManagerApplicationEntity save(ManagerApplicationEntity entity) {
		entity = jpa.save(entity);
		
		// 同时在菜单中插入一条新的应用数据
		ManagerResourceEntity e = new ManagerResourceEntity(); 
		e.setResourceName(entity.getApplicationName());
		e.setMenuType(MenuEnums.MENU_PLATFORM.value);
		e.setResourceParent(ResourceTypeEnmus.PLATFORM_RESOURCE_PARENT.value);
		e.setApplicationId(entity.getId());
		e.setTenantId(entity.getTenantId());
		e.setResourceLink(entity.getApplicationLink()) ; 
		
		managerResourceService.save(e) ; 
		return entity ; 
	}

	@Override
	public List<ManagerApplicationEntity> findAllByAccountId(String accountId) {
		log.debug("accountId:{}" , accountId);
		
		Optional<ManagerAccountEntity> as =  managerAccountService.findById(accountId) ; 
		
		if(as.isPresent()) {
			
			List<ManagerApplicationEntity> list = null ; 
			ManagerAccountEntity account = as.get() ; 
			
			if(MenuEnums.MENU_PLATFORM.value.equals(account.getRolePower())) {
				list = findAll() ; 
			} else {
				
				List<ManagerAccountRoleEntity> roleEntityList = managerAccountRoleService.findAllByAccountId(accountId) ; 
				
				Map<String, ManagerAccountRoleEntity> applicationMap = new HashMap<String , ManagerAccountRoleEntity>() ; 
				for(ManagerAccountRoleEntity m : roleEntityList) {
					applicationMap.put(m.getRoleId(), m) ; 
				}
				
				List<ManagerRoleEntity> roleList = managerRoleService.findAllById(applicationMap.keySet()) ; 
				Map<String, ManagerRoleEntity> roleListMap = new HashMap<String , ManagerRoleEntity>() ; 
				for(ManagerRoleEntity m : roleList) {
					roleListMap.put(m.getApplicationId(), m) ; 
				}
				
				list = findAllById(roleListMap.keySet()) ; 
			}
			
			return list ; 
		}
		
		return null;
	}
	
}
