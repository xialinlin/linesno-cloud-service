package com.alinesno.cloud.base.boot.service;

import java.util.List;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.base.boot.entity.ManagerDepartmentEntity;
import com.alinesno.cloud.base.boot.repository.ManagerDepartmentRepository;
import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.common.facade.wrapper.RestWrapper;

/**
 * <p>  服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-03-24 13:24:58
 */
@NoRepositoryBean
public interface IManagerDepartmentService extends IBaseService<ManagerDepartmentRepository, ManagerDepartmentEntity, String> {

	List<ManagerDepartmentEntity> findAllWithApplication(RestWrapper restWrapper);

}
