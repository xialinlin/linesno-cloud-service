package com.alinesno.cloud.base.boot.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;


/**
 * <p>
 * 
 * </p>
 *
 * @author LuoAnDong
 * @since 2018-12-16 17:53:19
 */
@Entity
@Table(name="info_school")
public class InfoSchoolEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属者
     */
	private String owners;
    /**
     * 学校地址
     */
	@Column(name="school_address")
	private String schoolAddress;
    /**
     * 学校名称
     */
	@Column(name="school_name")
	private String schoolName;
    /**
     * 办学类型代码
     */
	@Column(name="school_type")
	private String schoolType;
    /**
     * 学校代码
     */
	@Column(name="school_code")
	private String schoolCode;
	@Column(name="school_province")
	private String schoolProvince;
	@Column(name="school_province_code")
	private String schoolProvinceCode;
    /**
     * 办学类型名称
     */
	@Column(name="school_type_name")
	private String schoolTypeName;
    /**
     * 学校性质类型
     */
	@Column(name="school_properties")
	private String schoolProperties;
    /**
     * 办学性质类型代码
     */
	@Column(name="school_properties_code")
	private String schoolPropertiesCode;
    /**
     * 学校举办者
     */
	@Column(name="school_owner")
	private String schoolOwner;
    /**
     * 学校举办者代码
     */
	@Column(name="school_owner_code")
	private String schoolOwnerCode;


	public String getOwners() {
		return owners;
	}

	public void setOwners(String owners) {
		this.owners = owners;
	}

	public String getSchoolAddress() {
		return schoolAddress;
	}

	public void setSchoolAddress(String schoolAddress) {
		this.schoolAddress = schoolAddress;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getSchoolType() {
		return schoolType;
	}

	public void setSchoolType(String schoolType) {
		this.schoolType = schoolType;
	}

	public String getSchoolCode() {
		return schoolCode;
	}

	public void setSchoolCode(String schoolCode) {
		this.schoolCode = schoolCode;
	}

	public String getSchoolProvince() {
		return schoolProvince;
	}

	public void setSchoolProvince(String schoolProvince) {
		this.schoolProvince = schoolProvince;
	}

	public String getSchoolProvinceCode() {
		return schoolProvinceCode;
	}

	public void setSchoolProvinceCode(String schoolProvinceCode) {
		this.schoolProvinceCode = schoolProvinceCode;
	}

	public String getSchoolTypeName() {
		return schoolTypeName;
	}

	public void setSchoolTypeName(String schoolTypeName) {
		this.schoolTypeName = schoolTypeName;
	}

	public String getSchoolProperties() {
		return schoolProperties;
	}

	public void setSchoolProperties(String schoolProperties) {
		this.schoolProperties = schoolProperties;
	}

	public String getSchoolPropertiesCode() {
		return schoolPropertiesCode;
	}

	public void setSchoolPropertiesCode(String schoolPropertiesCode) {
		this.schoolPropertiesCode = schoolPropertiesCode;
	}

	public String getSchoolOwner() {
		return schoolOwner;
	}

	public void setSchoolOwner(String schoolOwner) {
		this.schoolOwner = schoolOwner;
	}

	public String getSchoolOwnerCode() {
		return schoolOwnerCode;
	}

	public void setSchoolOwnerCode(String schoolOwnerCode) {
		this.schoolOwnerCode = schoolOwnerCode;
	}


	@Override
	public String toString() {
		return "InfoSchoolEntity{" +
			"owners=" + owners +
			", schoolAddress=" + schoolAddress +
			", schoolName=" + schoolName +
			", schoolType=" + schoolType +
			", schoolCode=" + schoolCode +
			", schoolProvince=" + schoolProvince +
			", schoolProvinceCode=" + schoolProvinceCode +
			", schoolTypeName=" + schoolTypeName +
			", schoolProperties=" + schoolProperties +
			", schoolPropertiesCode=" + schoolPropertiesCode +
			", schoolOwner=" + schoolOwner +
			", schoolOwnerCode=" + schoolOwnerCode +
			"}";
	}
}
