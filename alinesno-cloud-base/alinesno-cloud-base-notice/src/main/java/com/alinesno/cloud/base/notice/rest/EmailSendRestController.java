package com.alinesno.cloud.base.notice.rest;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.notice.entity.EmailSendEntity;
import com.alinesno.cloud.base.notice.service.IEmailSendService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Scope("prototype")
@RestController
@RequestMapping("emailSend")
public class EmailSendRestController extends BaseRestController<EmailSendEntity , IEmailSendService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger logger = LoggerFactory.getLogger(EmailSendRestController.class);

	private IEmailSendService emailSendService ; 
	
	@Autowired
    private ApplicationContext applicationContext;

	/**
	 * 发送纯文本邮件
	 * @param strategy 发送策略
	 * @param emailEntity
	 */
	@PostMapping("sendTextEmail")
	public ResponseEntity<EmailSendEntity> sendTextEmail(@RequestParam String strategy ,  @RequestBody EmailSendEntity email) {
		emailSendService = (IEmailSendService) applicationContext.getBean(strategy+"EmailService") ; 
		return emailSendService.sendHtmlEmail(email) ; 
	}
	
	/**
	 * 发送超文件邮件(HTML)
	 * @param strategy 发送策略
	 * @param emailEntity
	 * @return
	 */
	@PostMapping("sendHtmlEmail")
	public ResponseEntity<EmailSendEntity> sendHtmlEmail(@RequestParam String strategy , @RequestBody EmailSendEntity email) {
		emailSendService = (IEmailSendService) applicationContext.getBean(strategy+"EmailService") ; 
		return emailSendService.sendHtmlEmail(email) ; 
	}

	/**
	 * 发送模板邮件
	 * @param strategy 发送策略
	 * @param maps 属性值 
	 * @param templateName 模板名称
	 * @param emailEntity 邮件信息
	 * @return
	 */
	@PostMapping("sendTemplateEmail")
	public ResponseEntity<EmailSendEntity> sendTemplateEmail(@RequestParam String strategy , Map<String, Object> maps , @RequestParam String templateName , @RequestBody  EmailSendEntity email) {
		emailSendService = (IEmailSendService) applicationContext.getBean(strategy+"EmailService") ; 
		return emailSendService.sendTemplateEmail(maps , templateName , email) ; 
	}
	
}
