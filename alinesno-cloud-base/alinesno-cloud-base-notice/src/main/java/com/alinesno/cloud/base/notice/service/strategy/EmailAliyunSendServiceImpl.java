package com.alinesno.cloud.base.notice.service.strategy;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.base.notice.entity.EmailSendEntity;
import com.alinesno.cloud.base.notice.repository.EmailSendRepository;
import com.alinesno.cloud.base.notice.service.IEmailSendService;
import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dm.model.v20151123.SingleSendMailRequest;
import com.aliyuncs.dm.model.v20151123.SingleSendMailResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2018-12-02 16:08:00
 */
@Service("aliyunEmailSendService")
public class EmailAliyunSendServiceImpl extends IBaseServiceImpl<EmailSendRepository, EmailSendEntity, String> implements IEmailSendService {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(EmailAliyunSendServiceImpl.class);

	@Value("${aliyun.email.access-key}")
	private String aliyunKey ;  //key

	@Value("${aliyun.email.access-key-secret}")
	private String aliyunSecret;  //密钥

	@Value("${aliyun.email.account-name}")
	private String emailAccountName ; //邮件发送地址

	@Value("${aliyun.email.tag}")
	private String emailTag ; //邮件标签

	@Value("${aliyun.email.single-max}")
	private int singleMaxSend ; //邮件标签

	@Value("${aliyun.email.from-alias}")
	private String fromAlias ; //邮件标签
	
	@Override
	public ResponseEntity<EmailSendEntity> sendTextEmail(EmailSendEntity email) {
		List<EmailSendEntity> emails = new ArrayList<EmailSendEntity>() ; 
		emails.add(email) ; 
		return this.sendBatchHtmlEmail(emails, email.getEmailSubject(), email.getEmailContent());
	}

	@Override
	public ResponseEntity<EmailSendEntity> sendHtmlEmail(EmailSendEntity email) {
		List<EmailSendEntity> emails = new ArrayList<EmailSendEntity>() ; 
		emails.add(email) ; 
		return this.sendBatchHtmlEmail(emails, email.getEmailSubject(), email.getEmailContent());
	}

	@Override
	public ResponseEntity<EmailSendEntity> sendTemplateEmail(Map<String, Object> maps , String templateName , EmailSendEntity email) {
		return null;
	}

	@Override
	public ResponseEntity<EmailSendEntity> sendBatchTextEmail(List<EmailSendEntity> emails , String subject , String htmlBody) {
		return this.sendBatchHtmlEmail(emails, subject, htmlBody);
	}

	@Override
	public ResponseEntity<EmailSendEntity> sendBatchHtmlEmail(List<EmailSendEntity> emails , String subject , String htmlBody) {
		
		IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", aliyunKey ,  aliyunSecret);
		IAcsClient client = new DefaultAcsClient(profile);

		SingleSendMailRequest request = new SingleSendMailRequest();
		String[] es = new String[emails.size()] ; 

		for(int i = 0 ; i < emails.size() ; i ++) {
			es[i] = emails.get(i).getEmailReceiver() ; 
		}
		
		try {
			request.setAccountName(emailAccountName);
			request.setFromAlias(fromAlias);
			request.setAddressType(1);
			request.setTagName(emailTag);
			request.setReplyToAddress(true);
			request.setToAddress(arrayToList(es));
			request.setSubject(subject);
			request.setHtmlBody(htmlBody);

			SingleSendMailResponse httpResponse = client.getAcsResponse(request);

			log.debug("email response = {}" , ToStringBuilder.reflectionToString(httpResponse));
			
		} catch (ClientException e) {
			log.debug("邮件发送异常:{}" , e.getMessage());
		}
		
		return null;
	}

	/**
	 * 数组转换成邮件
	 * @param email
	 * @return
	 */
	private String arrayToList(String[] email) {
		String emailStr = "" ;
		int length = email.length>singleMaxSend?singleMaxSend:email.length ;
		for(int i = 0  ; i < length ; i ++) {
			emailStr += email[i] ;
			if(i != email.length -1) {
				emailStr += "," ;
			}
		}
		log.debug("邮件列表:{}" , emailStr);
		return emailStr ;
	}
}
