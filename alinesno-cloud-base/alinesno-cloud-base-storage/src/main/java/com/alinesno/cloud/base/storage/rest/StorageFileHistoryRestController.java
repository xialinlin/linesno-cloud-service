package com.alinesno.cloud.base.storage.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.alinesno.cloud.base.storage.entity.StorageFileHistoryEntity;
import com.alinesno.cloud.base.storage.service.IStorageFileHistoryService;
import com.alinesno.cloud.common.core.rest.BaseRestController;

/**
 * <p> 接口 </p>
 *
 * @author LuoAnDong
 * @since 2019-04-09 20:22:17
 */
@Scope("prototype")
@RestController
@RequestMapping("storageFileHistory")
public class StorageFileHistoryRestController extends BaseRestController<StorageFileHistoryEntity , IStorageFileHistoryService> {

	//日志记录
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(StorageFileHistoryRestController.class);

}
