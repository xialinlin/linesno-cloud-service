package com.alinesno.cloud.base.storage.api;

import static org.junit.Assert.fail;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.alinesno.cloud.base.storage.feign.dto.StorageFileDto;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StorageFileClientTest {

	private static final Logger log = LoggerFactory.getLogger(StorageFileClientTest.class) ; 
	
	@Autowired
	private StorageFileClient storageFileClient ; 

	@Test
	public void testUploadDataStringString() throws ClientProtocolException, IOException {
		log.debug("storage file client:{}" , storageFileClient);
		
		String localFile = "/Users/luodong/Desktop/demo_05.jpg" ; 
		StorageFileDto dto = storageFileClient.uploadData(localFile) ; 
		log.debug("StorageFileDto:{}" , dto);
	}

	@Test
	public void testUploadDataString() {
		fail("Not yet implemented");
	}

	@Test
	public void testDownloadData() {
		fail("Not yet implemented");
	}

	@Test
	public void testDeleteData() {
		fail("Not yet implemented");
	}

}
