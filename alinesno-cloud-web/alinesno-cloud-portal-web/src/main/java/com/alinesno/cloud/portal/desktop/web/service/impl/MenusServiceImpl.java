package com.alinesno.cloud.portal.desktop.web.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alinesno.cloud.common.core.services.impl.IBaseServiceImpl;
import com.alinesno.cloud.portal.desktop.web.bean.MenusBean;
import com.alinesno.cloud.portal.desktop.web.bean.ModuleBean;
import com.alinesno.cloud.portal.desktop.web.entity.MenusEntity;
import com.alinesno.cloud.portal.desktop.web.enums.HasNavEmnus;
import com.alinesno.cloud.portal.desktop.web.repository.MenusRepository;
import com.alinesno.cloud.portal.desktop.web.service.IMenusService;
import com.alinesno.cloud.portal.desktop.web.service.IModuleService;

import cn.hutool.core.bean.BeanUtil;

/**
 * <p>  服务实现类 </p>
 *
 * @author LuoAnDong
 * @since 2019-05-18 14:46:04
 */
@Service
public class MenusServiceImpl extends IBaseServiceImpl<MenusRepository, MenusEntity, String> implements IMenusService {

	//日志记录
	private static final Logger log = LoggerFactory.getLogger(MenusServiceImpl.class);

	@Autowired
	private IModuleService moduleService ; 

	@Override
	public List<MenusBean> findNavigation() {
		log.debug("查询主导航");
		List<MenusBean> menusBean = new ArrayList<MenusBean>() ; 
		
		List<MenusEntity> list = jpa.findAllByHasNavOrderByMenusSortAsc(HasNavEmnus.NAV.getValue());
	
		for(MenusEntity e : list) {
			MenusBean b = new MenusBean() ; 
			BeanUtil.copyProperties(e, b);

			List<ModuleBean> modules = moduleService.findAllByMenusId(e.getId()) ; 
			b.setHasModule((modules!=null&&modules.size()>0)?true:false);
			b.setModules(modules); 
			
			menusBean.add(b) ; 
		}
		
		return menusBean ; 
	}

}
