package com.alinesno.cloud.demo.single.web.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.alinesno.cloud.common.core.orm.entity.BaseEntity;

/**
 * 日志记录
 * 
 * @author LuoAnDong
 * @since 2018年8月5日 下午12:37:44
 */
@SuppressWarnings("serial")
@Entity
@Table(name="logger")
public class LoggerEntity extends BaseEntity {

	private String recordMsg; // 记录时间,
	private String recordChannel; // 记录渠道,
	private String recordParams; // 日志参数,
	private String recordUser; // 用户名称,
	private String recordUserName; // 日志用户名,
	private String recordType; // 日志记录类型,
	
	public String getRecordMsg() {
		return recordMsg;
	}

	public void setRecordMsg(String recordMsg) {
		this.recordMsg = recordMsg;
	}

	public String getRecordChannel() {
		return recordChannel;
	}

	public void setRecordChannel(String recordChannel) {
		this.recordChannel = recordChannel;
	}

	public String getRecordParams() {
		return recordParams;
	}

	public void setRecordParams(String recordParams) {
		this.recordParams = recordParams;
	}

	public String getRecordUser() {
		return recordUser;
	}

	public void setRecordUser(String recordUser) {
		this.recordUser = recordUser;
	}

	public String getRecordUserName() {
		return recordUserName;
	}

	public void setRecordUserName(String recordUserName) {
		this.recordUserName = recordUserName;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

}
