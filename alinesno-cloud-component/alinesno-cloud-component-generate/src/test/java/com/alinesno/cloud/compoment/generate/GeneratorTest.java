package com.alinesno.cloud.compoment.generate;

/**
 * 生成代码
 * 
 * @author luodong
 *
 */
public class GeneratorTest extends Generator {

	@Override
	public void initTableName() {
	}

	public static void main(String[] args) {
		GeneratorTest g = new GeneratorTest();

		// 启动服务
//		g.feiginServer = "alinesno-cloud-base-boot" ; 
//		g.parentPackage="com.alinesno.cloud";
//		g.generator("boot"); 
		
		// 打印服务
//		g.feiginServer = "alinesno-cloud-base-print" ; 
//		g.parentPackage="com.alinesno.cloud";
//		g.generator("print"); 
		
//		g.feiginServer = "alinesno-cloud-business-crm" ; 
//		g.parentPackage="com.alinesno.cloud";
//		g.generator("business.crm"); 

//		g.feiginServer = "alinesno-cloud-portal-desktop-web" ; 
//		g.parentPackage="com.alinesno.cloud";
//		g.generator("portal"); 
		
		// 日志服务
//		g.feiginServer = "alinesno-cloud-base-logger" ; 
//		g.parentPackage="com.alinesno.cloud";
//		g.generator("logger"); 
		
		// 通知服务 
//		g.feiginServer = "alinesno-cloud-base-notice" ; 
//		g.parentPackage="com.alinesno.cloud";
//		g.generator("notice"); 

		// 消息应用
//		g.feiginServer = "alinesno-cloud-base-message" ; 
//		g.parentPackage="com.alinesno.cloud";
//		g.isOpenFile = false ;
//		g.generator("message"); 
		
		// 微信应用
//		g.feiginServer = "alinesno-cloud-base-wechat" ; 
//		g.parentPackage="com.alinesno.cloud.base.wechat";
//		g.generator("wechat"); 
	
		// 定时任务 
//		g.feiginServer = "alinesno-cloud-base-task" ; 
//		g.parentPackage="com.alinesno.cloud.base.task";
//		g.generator("task"); 
		
		// 存储服务
//		g.feiginServer = "alinesno-cloud-base-storage" ; 
//		g.parentPackage="com.alinesno.cloud";
//		g.generator("storage"); 
		
		// 代码生成器 
		g.feiginServer = "alinesno-cloud-compoment-code" ; 
		g.parentPackage="com.alinesno.cloud";
		g.generator("code"); 

	}

}
