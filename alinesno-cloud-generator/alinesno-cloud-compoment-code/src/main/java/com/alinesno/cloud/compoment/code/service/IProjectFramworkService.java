package com.alinesno.cloud.compoment.code.service;

import org.springframework.data.repository.NoRepositoryBean;

import com.alinesno.cloud.common.core.services.IBaseService;
import com.alinesno.cloud.compoment.code.entity.ProjectFramworkEntity;
import com.alinesno.cloud.compoment.code.repository.ProjectFramworkRepository;

/**
 * <p> 项目应用技术 服务类 </p>
 *
 * @author LuoAnDong
 * @since 2019-06-29 12:19:41
 */
@NoRepositoryBean
public interface IProjectFramworkService extends IBaseService<ProjectFramworkRepository, ProjectFramworkEntity, String> {

}
