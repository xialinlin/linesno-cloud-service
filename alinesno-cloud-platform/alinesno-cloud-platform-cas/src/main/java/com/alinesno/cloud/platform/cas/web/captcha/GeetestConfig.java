package com.alinesno.cloud.platform.cas.web.captcha;

/**
 * GeetestWeb配置文件
 * 
 *
 */
public class GeetestConfig {

	// 填入自己的captcha_id和private_key
	private static final String geetest_id = "bef167542f2d399670803316abcf356a";
	private static final String geetest_key = "1ad0937d9d32455712e3526fe88c3dff";
	private static final boolean newfailback = true;

	public static final String getGeetest_id() {
		return geetest_id;
	}

	public static final String getGeetest_key() {
		return geetest_key;
	}
	
	public static final boolean isnewfailback() {
		return newfailback;
	}

}
